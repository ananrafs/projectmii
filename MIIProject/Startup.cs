﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MIIProject.Startup))]
namespace MIIProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
