﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Asuransi.Startup))]
namespace Asuransi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
