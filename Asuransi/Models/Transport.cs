﻿using Asuransi.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Asuransi.Models
{
    public class Transport : Metadata
    {
        [Key]
        public string Id { get; set; }
        public string STNK { get; set; }
        public string Variant { get; set; }
        public DateTimeOffset Year { get; set; }
        public virtual ApplicationUser Owner { get; set; }
    }

    public class Patient : Metadata
    {
        [Key]
        public string Id { get; set; }
        public string KTP { get; set; }
        public string KK { get; set; }
        public virtual ApplicationUser Owner { get; set; }
        public DateTimeOffset DoB { get; set; }
        public string Gender { get; set; }

    }
    public class TransportInsurance : Metadata
    {
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }
        public int Premi { get; set; }
        public int Duration { get; set; }
        public int ClaimSparePart { get; set; }
        public int ClaimRepair { get; set; }

    }

    public class PatientInsurance : Metadata
    {
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }
        public int Premi { get; set; }
        public int Duration { get; set; }
        public int ClaimInpatient { get; set; }
        public int ClaimMedicine { get; set; }
        public int ClaimAmbulance { get; set; }
        public int ClaimDoctor { get; set; }

    }

    public class UserTransportInsurance : Metadata
    {
        [Key]
        public string Id { get; set; }
        public TransportInsurance TransportInsurance { get; set; }
        public Transport Transport { get; set; }
        public DateTimeOffset? Expired { get; set; }
        public Status Status { get; set; }
    }
    public class UserPatientInsurance : Metadata
    {
        [Key]
        public string Id { get; set; }
        public Patient Patient { get; set; }
        public PatientInsurance PatientInsurance { get; set; }
        public DateTimeOffset? Expired { get; set; }
        public Status Status { get; set; }


    }
    public class PayPatient : Metadata
    {
        [Key]
        public string Id { get; set; }
        public UserPatientInsurance PatientInsuranceDetail { get; set; }
        public DateTimeOffset Month { get; set; }
        public int Fine { get; set; }
        public int Pay { get; set; }

    }

    public class PayTransport : Metadata
    {
        [Key]
        public string Id { get; set; }
        public UserTransportInsurance TransportInsuranceDetail { get; set; }
        public DateTimeOffset Month { get; set; }
        public int Pay { get; set; }
        public int Fine { get; set; }

    }

    public class ClaimCostPatient : Metadata
    {
        [Key]
        public string Id { get; set; }
        public string HospitalInvoice { get; set; }
        public UserPatientInsurance PatientInsuranceDetail { get; set; }
        public string IdCard { get; set; }
        public string LabResult { get; set; }
        public ClaimStatus ClaimStatus { get; set; }

    }
    public class ClaimCostTransport : Metadata
    {
        [Key]
        public string Id { get; set; }
        public UserTransportInsurance TransportInsuranceDetail { get; set; }
        public string RepairInvoice { get; set; }
        public ClaimStatus ClaimStatus { get; set; }

    }

    public class Metadata
    {
        public DateTimeOffset? Created { get; set; }
        public virtual ApplicationUser CreatedBy { get; set; }
        public bool isDelete { get; set; }
        public DateTimeOffset? Deleted { get; set; }
        public virtual ApplicationUser DeletedBy { get; set; }
        public DateTimeOffset? Updated { get; set; }
        public virtual ApplicationUser UpdatedBy { get; set; }

    }
    public enum Status
    {
        Submitted,
        Approved,
        Rejected,
        Complicated,
        GracePeriod,
        Extended,
        Expired
    }
    public enum ClaimStatus
    {
        Submitted,
        Claimed,
        Rejected
    }

}
