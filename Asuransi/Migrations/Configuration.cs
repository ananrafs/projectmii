namespace Asuransi.Migrations
{
    using Models;
    using Microsoft.AspNet.Identity;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Microsoft.AspNet.Identity.EntityFramework;

    internal sealed class Configuration : DbMigrationsConfiguration<Asuransi.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Asuransi.Models.ApplicationDbContext context)
        {
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

            var roleAdmin = roleManager.FindByName("Admin");
            if (roleAdmin == null)
            {
                roleManager.Create(new IdentityRole { Name = "Admin" });
            }
            var roleKaryawan = roleManager.FindByName("Karyawan");
            if (roleKaryawan == null)
            {
                roleManager.Create(new IdentityRole { Name = "Karyawan" });
            }
            var roleUser = roleManager.FindByName("User");
            if (roleUser == null)
            {
                roleManager.Create(new IdentityRole { Name = "User" });
            }
            //var author = new ApplicationUser
            //{
            //    PhoneNumber = "+6282311028592",
            //    PhoneNumberConfirmed = true,
            //    UserName = "author2@training.id",
            //    Email = "author2@training.id"
            //};
            //if (manager.FindByName("author2@training.id") == null)
            //{
            //    manager.Create(author, "Training@2018");
            //    manager.AddToRoles(author.Id, new string[] { "Author" });
            //}
            //var editor = new ApplicationUser
            //{
            //    PhoneNumber = "+6282311028592",
            //    PhoneNumberConfirmed = true,
            //    UserName = "editor@training.id",
            //    Email = "editor@training.id"
            //};
            //if (manager.FindByName("editor@training.id") == null)
            //{
            //    manager.Create(editor, "Training@2018");
            //    manager.AddToRoles(editor.Id, new string[] { "Editor" });
            //}
        
    }
    }
}
