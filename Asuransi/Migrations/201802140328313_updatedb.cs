namespace Asuransi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Patients", "Owner_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.Transports", "Owner_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Patients", "Owner_Id");
            CreateIndex("dbo.Transports", "Owner_Id");
            AddForeignKey("dbo.Patients", "Owner_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Transports", "Owner_Id", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.Patients", "Name");
            DropColumn("dbo.Transports", "Owner");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Transports", "Owner", c => c.String());
            AddColumn("dbo.Patients", "Name", c => c.String());
            DropForeignKey("dbo.Transports", "Owner_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Patients", "Owner_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Transports", new[] { "Owner_Id" });
            DropIndex("dbo.Patients", new[] { "Owner_Id" });
            DropColumn("dbo.Transports", "Owner_Id");
            DropColumn("dbo.Patients", "Owner_Id");
        }
    }
}
