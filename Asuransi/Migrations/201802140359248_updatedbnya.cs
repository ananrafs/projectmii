namespace Asuransi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedbnya : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Patients", "KTP", c => c.String());
            AddColumn("dbo.Patients", "KK", c => c.String());
            AddColumn("dbo.Transports", "STNK", c => c.String());
            AlterColumn("dbo.UserPatientInsurances", "Expired", c => c.DateTimeOffset(precision: 7));
            AlterColumn("dbo.UserTransportInsurances", "Expired", c => c.DateTimeOffset(precision: 7));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserTransportInsurances", "Expired", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.UserPatientInsurances", "Expired", c => c.DateTimeOffset(nullable: false, precision: 7));
            DropColumn("dbo.Transports", "STNK");
            DropColumn("dbo.Patients", "KK");
            DropColumn("dbo.Patients", "KTP");
        }
    }
}
