namespace Asuransi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createdb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClaimCostPatients",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        HospitalInvoice = c.String(),
                        IdCard = c.String(),
                        LabResult = c.String(),
                        ClaimStatus = c.Int(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        isDelete = c.Boolean(nullable: false),
                        Deleted = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        PatientInsuranceDetail_Id = c.String(maxLength: 128),
                        UpdatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DeletedBy_Id)
                .ForeignKey("dbo.UserPatientInsurances", t => t.PatientInsuranceDetail_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy_Id)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.DeletedBy_Id)
                .Index(t => t.PatientInsuranceDetail_Id)
                .Index(t => t.UpdatedBy_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.UserPatientInsurances",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Expired = c.DateTimeOffset(nullable: false, precision: 7),
                        Status = c.Int(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        isDelete = c.Boolean(nullable: false),
                        Deleted = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        Patient_Id = c.String(maxLength: 128),
                        PatientInsurance_Id = c.String(maxLength: 128),
                        UpdatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DeletedBy_Id)
                .ForeignKey("dbo.Patients", t => t.Patient_Id)
                .ForeignKey("dbo.PatientInsurances", t => t.PatientInsurance_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy_Id)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.DeletedBy_Id)
                .Index(t => t.Patient_Id)
                .Index(t => t.PatientInsurance_Id)
                .Index(t => t.UpdatedBy_Id);
            
            CreateTable(
                "dbo.Patients",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        DoB = c.DateTimeOffset(nullable: false, precision: 7),
                        Gender = c.String(),
                        Created = c.DateTimeOffset(precision: 7),
                        isDelete = c.Boolean(nullable: false),
                        Deleted = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        UpdatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DeletedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy_Id)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.DeletedBy_Id)
                .Index(t => t.UpdatedBy_Id);
            
            CreateTable(
                "dbo.PatientInsurances",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        Premi = c.Int(nullable: false),
                        Duration = c.Int(nullable: false),
                        ClaimInpatient = c.Int(nullable: false),
                        ClaimMedicine = c.Int(nullable: false),
                        ClaimAmbulance = c.Int(nullable: false),
                        ClaimDoctor = c.Int(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        isDelete = c.Boolean(nullable: false),
                        Deleted = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        UpdatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DeletedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy_Id)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.DeletedBy_Id)
                .Index(t => t.UpdatedBy_Id);
            
            CreateTable(
                "dbo.ClaimCostTransports",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        RepairInvoice = c.String(),
                        ClaimStatus = c.Int(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        isDelete = c.Boolean(nullable: false),
                        Deleted = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        TransportInsuranceDetail_Id = c.String(maxLength: 128),
                        UpdatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DeletedBy_Id)
                .ForeignKey("dbo.UserTransportInsurances", t => t.TransportInsuranceDetail_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy_Id)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.DeletedBy_Id)
                .Index(t => t.TransportInsuranceDetail_Id)
                .Index(t => t.UpdatedBy_Id);
            
            CreateTable(
                "dbo.UserTransportInsurances",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Expired = c.DateTimeOffset(nullable: false, precision: 7),
                        Status = c.Int(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        isDelete = c.Boolean(nullable: false),
                        Deleted = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        Transport_Id = c.String(maxLength: 128),
                        TransportInsurance_Id = c.String(maxLength: 128),
                        UpdatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DeletedBy_Id)
                .ForeignKey("dbo.Transports", t => t.Transport_Id)
                .ForeignKey("dbo.TransportInsurances", t => t.TransportInsurance_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy_Id)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.DeletedBy_Id)
                .Index(t => t.Transport_Id)
                .Index(t => t.TransportInsurance_Id)
                .Index(t => t.UpdatedBy_Id);
            
            CreateTable(
                "dbo.Transports",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Variant = c.String(),
                        Year = c.DateTimeOffset(nullable: false, precision: 7),
                        Owner = c.String(),
                        Created = c.DateTimeOffset(precision: 7),
                        isDelete = c.Boolean(nullable: false),
                        Deleted = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        UpdatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DeletedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy_Id)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.DeletedBy_Id)
                .Index(t => t.UpdatedBy_Id);
            
            CreateTable(
                "dbo.TransportInsurances",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        Premi = c.Int(nullable: false),
                        Duration = c.Int(nullable: false),
                        ClaimSparePart = c.Int(nullable: false),
                        ClaimRepair = c.Int(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        isDelete = c.Boolean(nullable: false),
                        Deleted = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        UpdatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DeletedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy_Id)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.DeletedBy_Id)
                .Index(t => t.UpdatedBy_Id);
            
            CreateTable(
                "dbo.PayPatients",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Month = c.DateTimeOffset(nullable: false, precision: 7),
                        Fine = c.Int(nullable: false),
                        Pay = c.Int(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        isDelete = c.Boolean(nullable: false),
                        Deleted = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        PatientInsuranceDetail_Id = c.String(maxLength: 128),
                        UpdatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DeletedBy_Id)
                .ForeignKey("dbo.UserPatientInsurances", t => t.PatientInsuranceDetail_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy_Id)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.DeletedBy_Id)
                .Index(t => t.PatientInsuranceDetail_Id)
                .Index(t => t.UpdatedBy_Id);
            
            CreateTable(
                "dbo.PayTransports",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Month = c.DateTimeOffset(nullable: false, precision: 7),
                        Pay = c.Int(nullable: false),
                        Fine = c.Int(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        isDelete = c.Boolean(nullable: false),
                        Deleted = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        TransportInsuranceDetail_Id = c.String(maxLength: 128),
                        UpdatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DeletedBy_Id)
                .ForeignKey("dbo.UserTransportInsurances", t => t.TransportInsuranceDetail_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy_Id)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.DeletedBy_Id)
                .Index(t => t.TransportInsuranceDetail_Id)
                .Index(t => t.UpdatedBy_Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.PayTransports", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.PayTransports", "TransportInsuranceDetail_Id", "dbo.UserTransportInsurances");
            DropForeignKey("dbo.PayTransports", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.PayTransports", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.PayPatients", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.PayPatients", "PatientInsuranceDetail_Id", "dbo.UserPatientInsurances");
            DropForeignKey("dbo.PayPatients", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.PayPatients", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClaimCostTransports", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClaimCostTransports", "TransportInsuranceDetail_Id", "dbo.UserTransportInsurances");
            DropForeignKey("dbo.UserTransportInsurances", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserTransportInsurances", "TransportInsurance_Id", "dbo.TransportInsurances");
            DropForeignKey("dbo.TransportInsurances", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.TransportInsurances", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.TransportInsurances", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserTransportInsurances", "Transport_Id", "dbo.Transports");
            DropForeignKey("dbo.Transports", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Transports", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Transports", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserTransportInsurances", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserTransportInsurances", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClaimCostTransports", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClaimCostTransports", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClaimCostPatients", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClaimCostPatients", "PatientInsuranceDetail_Id", "dbo.UserPatientInsurances");
            DropForeignKey("dbo.UserPatientInsurances", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserPatientInsurances", "PatientInsurance_Id", "dbo.PatientInsurances");
            DropForeignKey("dbo.PatientInsurances", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.PatientInsurances", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.PatientInsurances", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserPatientInsurances", "Patient_Id", "dbo.Patients");
            DropForeignKey("dbo.Patients", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Patients", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Patients", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserPatientInsurances", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserPatientInsurances", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClaimCostPatients", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClaimCostPatients", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.PayTransports", new[] { "UpdatedBy_Id" });
            DropIndex("dbo.PayTransports", new[] { "TransportInsuranceDetail_Id" });
            DropIndex("dbo.PayTransports", new[] { "DeletedBy_Id" });
            DropIndex("dbo.PayTransports", new[] { "CreatedBy_Id" });
            DropIndex("dbo.PayPatients", new[] { "UpdatedBy_Id" });
            DropIndex("dbo.PayPatients", new[] { "PatientInsuranceDetail_Id" });
            DropIndex("dbo.PayPatients", new[] { "DeletedBy_Id" });
            DropIndex("dbo.PayPatients", new[] { "CreatedBy_Id" });
            DropIndex("dbo.TransportInsurances", new[] { "UpdatedBy_Id" });
            DropIndex("dbo.TransportInsurances", new[] { "DeletedBy_Id" });
            DropIndex("dbo.TransportInsurances", new[] { "CreatedBy_Id" });
            DropIndex("dbo.Transports", new[] { "UpdatedBy_Id" });
            DropIndex("dbo.Transports", new[] { "DeletedBy_Id" });
            DropIndex("dbo.Transports", new[] { "CreatedBy_Id" });
            DropIndex("dbo.UserTransportInsurances", new[] { "UpdatedBy_Id" });
            DropIndex("dbo.UserTransportInsurances", new[] { "TransportInsurance_Id" });
            DropIndex("dbo.UserTransportInsurances", new[] { "Transport_Id" });
            DropIndex("dbo.UserTransportInsurances", new[] { "DeletedBy_Id" });
            DropIndex("dbo.UserTransportInsurances", new[] { "CreatedBy_Id" });
            DropIndex("dbo.ClaimCostTransports", new[] { "UpdatedBy_Id" });
            DropIndex("dbo.ClaimCostTransports", new[] { "TransportInsuranceDetail_Id" });
            DropIndex("dbo.ClaimCostTransports", new[] { "DeletedBy_Id" });
            DropIndex("dbo.ClaimCostTransports", new[] { "CreatedBy_Id" });
            DropIndex("dbo.PatientInsurances", new[] { "UpdatedBy_Id" });
            DropIndex("dbo.PatientInsurances", new[] { "DeletedBy_Id" });
            DropIndex("dbo.PatientInsurances", new[] { "CreatedBy_Id" });
            DropIndex("dbo.Patients", new[] { "UpdatedBy_Id" });
            DropIndex("dbo.Patients", new[] { "DeletedBy_Id" });
            DropIndex("dbo.Patients", new[] { "CreatedBy_Id" });
            DropIndex("dbo.UserPatientInsurances", new[] { "UpdatedBy_Id" });
            DropIndex("dbo.UserPatientInsurances", new[] { "PatientInsurance_Id" });
            DropIndex("dbo.UserPatientInsurances", new[] { "Patient_Id" });
            DropIndex("dbo.UserPatientInsurances", new[] { "DeletedBy_Id" });
            DropIndex("dbo.UserPatientInsurances", new[] { "CreatedBy_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.ClaimCostPatients", new[] { "UpdatedBy_Id" });
            DropIndex("dbo.ClaimCostPatients", new[] { "PatientInsuranceDetail_Id" });
            DropIndex("dbo.ClaimCostPatients", new[] { "DeletedBy_Id" });
            DropIndex("dbo.ClaimCostPatients", new[] { "CreatedBy_Id" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.PayTransports");
            DropTable("dbo.PayPatients");
            DropTable("dbo.TransportInsurances");
            DropTable("dbo.Transports");
            DropTable("dbo.UserTransportInsurances");
            DropTable("dbo.ClaimCostTransports");
            DropTable("dbo.PatientInsurances");
            DropTable("dbo.Patients");
            DropTable("dbo.UserPatientInsurances");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.ClaimCostPatients");
        }
    }
}
