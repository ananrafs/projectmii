﻿using Asuransi.Repo.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Asuransi.Models;
using System.Data.Entity;

namespace Asuransi.Repo.Repositories
{
    public class GeneralRepository<TEntity> : IGeneralRepo<TEntity> where TEntity : class
    {
        ApplicationDbContext cn;

        public GeneralRepository(ApplicationDbContext context)
        {
            cn = context;
        }
        public void Add(TEntity entity)
        {
            cn.Set<TEntity>().Add(entity);
        }

        public void Dispose()
        {
           cn.Dispose();
        }

        public async Task Edit(TEntity entity)
        {
            cn.Entry(entity).State = EntityState.Modified;
            await cn.SaveChangesAsync();
        }

        public IEnumerable<TEntity> getAll()
        {
            return cn.Set<TEntity>();
        }

        public async Task<IEnumerable<TEntity>> getAlltoList()
        {
            return await cn.Set<TEntity>().ToListAsync();
        }

        public async Task<TEntity> getById(string id)
        {
            return await cn.Set<TEntity>().FindAsync(id);
        }

        public IEnumerable<TEntity> getMany(Expression<Func<TEntity, bool>> predicate)
        {
            return cn.Set<TEntity>().Where(predicate);
        }

        public async Task<IEnumerable<TEntity>> getManytoList(Expression<Func<TEntity, bool>> predicate)
        {
            return await cn.Set<TEntity>().Where(predicate).ToListAsync();
        }

        public async Task<TEntity> getSingle(Expression<Func<TEntity, bool>> predicate)
        {
            return await cn.Set<TEntity>().Where(predicate).SingleOrDefaultAsync();
        }

        public async Task Remove(TEntity entity)
        {
            cn.Set<TEntity>().Remove(entity);
            await cn.SaveChangesAsync();
        }

    }
}