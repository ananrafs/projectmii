﻿using Asuransi.Models;
using Asuransi.Repo.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Asuransi.Repo.Repositories
{
    public class PatientRepository : GeneralRepository<Patient>, IUserRepo<Patient>
    {
        private ApplicationDbContext cn;
        public PatientRepository(ApplicationDbContext context) : base(context)
        {
            cn = context;
        }
    }

    public class PatientInsuranceRepository : GeneralRepository<PatientInsurance>, IInsuranceRepo<PatientInsurance>
    {
        private ApplicationDbContext cn;
        public PatientInsuranceRepository(ApplicationDbContext context) : base(context)
        {
            cn = context;
        }
    }
    public class PatientUserInsuranceRepository : GeneralRepository<UserPatientInsurance>, IInsuranceUserRepo<UserPatientInsurance>
    {
        private ApplicationDbContext cn;
        public PatientUserInsuranceRepository(ApplicationDbContext context) : base(context)
        {
            cn = context;
        }
    }

    public class PatientPayRepository : GeneralRepository<PayPatient>, IInsurancePayRepo<PayPatient>
    {
        private ApplicationDbContext cn;
        public PatientPayRepository(ApplicationDbContext context) : base(context)
        {
            cn = context;
        }
    }

    public class PatientClaimRepository : GeneralRepository<ClaimCostPatient>, IInsuranceClaimRepo<ClaimCostPatient>
    {
        private ApplicationDbContext cn;
        public PatientClaimRepository(ApplicationDbContext context) : base(context)
        {
            cn = context;
        }
    }
}