﻿using Asuransi.Models;
using Asuransi.Repo.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Asuransi.Repo.Repositories
{
    public class TransportRepository : GeneralRepository<Transport>, IUserRepo<Transport>
    {
        private ApplicationDbContext cn;
        public TransportRepository(ApplicationDbContext context) : base(context)
        {
            cn = context;
        }

    }

    public class TransportInsuranceRepository : GeneralRepository<TransportInsurance>, IInsuranceRepo<TransportInsurance>
    {
        private ApplicationDbContext cn;
        public TransportInsuranceRepository(ApplicationDbContext context) : base(context)
        {
            cn = context;
        }
    }
    public class TransportUserInsuranceRepository : GeneralRepository<UserTransportInsurance>, IInsuranceUserRepo<UserTransportInsurance>
    {
        private ApplicationDbContext cn;
        public TransportUserInsuranceRepository(ApplicationDbContext context) : base(context)
        {
            cn = context;
        }
    }

    public class TransportPayRepository : GeneralRepository<PayTransport>, IInsurancePayRepo<PayTransport>
    {
        private ApplicationDbContext cn;
        public TransportPayRepository(ApplicationDbContext context) : base(context)
        {
            cn = context;
        }
    }

    public class TransportClaimRepository : GeneralRepository<ClaimCostTransport>, IInsuranceClaimRepo<ClaimCostTransport>
    {
        private ApplicationDbContext cn;
        public TransportClaimRepository(ApplicationDbContext context) : base(context)
        {
            cn = context;
        }
    }
}