﻿using Asuransi.Models;
using Asuransi.Repo.Interface;
using Asuransi.Repo.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Asuransi.Repo
{
    public class UnitOfWork : IDisposable
    {
        private ApplicationDbContext cn;
        private PatientRepository PRepo;
        private PatientInsuranceRepository PIRepo;
        private PatientUserInsuranceRepository PUIRepo;
        private PatientPayRepository PPRepo;
        private PatientClaimRepository PCRepo;
        private TransportRepository TRepo;
        private TransportInsuranceRepository TIRepo;
        private TransportUserInsuranceRepository TUIRepo;
        private TransportPayRepository TPRepo;
        private TransportClaimRepository TCRepo;
        
        public UnitOfWork(ApplicationDbContext context)
        {
            cn = context;
        }

        public IUserRepo<Patient> PatientRepo
        {
            get
            {
                return this.PRepo ?? new PatientRepository(cn);
            }
        }
        public IUserRepo<Transport> TransportRepo
        {
            get
            {
                return this.TRepo ?? new TransportRepository(cn);
            }
        }
        public IInsuranceRepo<PatientInsurance> PatientInsRepo
        {
            get
            {
                return this.PIRepo ?? new PatientInsuranceRepository(cn);
            }
        }
        public IInsuranceRepo<TransportInsurance> TransInsRepo
        {
            get
            {
                return this.TIRepo ?? new TransportInsuranceRepository(cn);
            }
        }
        public IInsuranceUserRepo<UserPatientInsurance> PatientUserInsRepo
        {
            get
            {
                return this.PUIRepo ?? new PatientUserInsuranceRepository(cn);
            }
        }
        public IInsuranceUserRepo<UserTransportInsurance> TransUserInsRepo
        {
            get
            {
                return this.TUIRepo ?? new TransportUserInsuranceRepository(cn);
            }
        }
        public IInsurancePayRepo<PayPatient> PatientPayRepo
        {
            get
            {
                return this.PPRepo ?? new PatientPayRepository(cn);
            }
        }
        public IInsurancePayRepo<PayTransport> TransPayRepo
        {
            get
            {
                return this.TPRepo ?? new TransportPayRepository(cn);
            }
        } 
        public IInsuranceClaimRepo<ClaimCostPatient> PatientClaimRepo
        {
            get
            {
                return this.PCRepo ?? new PatientClaimRepository(cn);
            }
        }
        public IInsuranceClaimRepo<ClaimCostTransport> TransClaimRepo
        {
            get
            {
                return this.TCRepo ?? new TransportClaimRepository(cn);
            }
        }
        public void Dispose()
        {
            cn.Dispose();
        }
    }
}