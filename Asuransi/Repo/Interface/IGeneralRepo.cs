﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Asuransi.Repo.Interface
{
    public interface IGeneralRepo<TEntity> :IDisposable where TEntity: class
    {
        Task<IEnumerable<TEntity>> getAlltoList();

        Task<TEntity> getById(string id);
        Task<IEnumerable<TEntity>> getManytoList(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> getSingle(Expression<Func<TEntity, bool>> predicate);
        void Add(TEntity entity);
        Task Remove(TEntity entity);
        Task Edit(TEntity entity);
        IEnumerable<TEntity> getAll();

        IEnumerable<TEntity> getMany(Expression<Func<TEntity, bool>> predicate);

    }
}
