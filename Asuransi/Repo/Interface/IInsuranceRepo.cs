﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asuransi.Repo.Interface
{
    public interface IInsuranceRepo<TEntity>: IGeneralRepo<TEntity> where TEntity:class
    {
    }

    public interface IUserRepo<TEntity> : IGeneralRepo<TEntity> where TEntity : class
    {

    }

    public interface IInsuranceUserRepo<TEntity> : IGeneralRepo<TEntity> where TEntity : class
    {

    }

    public interface IInsurancePayRepo<TEntity> : IGeneralRepo<TEntity> where TEntity : class
    {

    }

    public interface IInsuranceClaimRepo<TEntity> : IGeneralRepo<TEntity> where TEntity : class
    {

    }
}
