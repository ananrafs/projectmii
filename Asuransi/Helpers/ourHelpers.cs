﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Asuransi.Helpers
{
    public static class ourHelpers
    {
        public static string UploadImage(HttpPostedFileBase file)
        {
            var path = "/Content/Images/uploads/";
            if (file == null || file.ContentLength == 0)
            {
                return null;
            }
            try
            {
                var fileName = Path.GetFileName(file.FileName);
                var resultPath = Path.Combine(HttpContext.Current.Server.MapPath(path), fileName);
                file.SaveAs(resultPath);
                return path + fileName;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError(ex.Message);
            }
            return null;
        }
    }
}