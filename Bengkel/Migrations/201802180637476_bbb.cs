namespace Bengkel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class bbb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NoPol = c.String(maxLength: 25, unicode: false),
                        Tahun = c.Int(nullable: false),
                        Owner = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        Deleted = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        UpdatedBy_Id = c.String(maxLength: 128),
                        Variant_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DeletedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy_Id)
                .ForeignKey("dbo.Variants", t => t.Variant_ID)
                .Index(t => t.NoPol, unique: true)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.DeletedBy_Id)
                .Index(t => t.UpdatedBy_Id)
                .Index(t => t.Variant_ID);
            
            AddColumn("dbo.Penjualans", "Customer_Id", c => c.Int());
            CreateIndex("dbo.Penjualans", "Customer_Id");
            AddForeignKey("dbo.Penjualans", "Customer_Id", "dbo.Customers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Customers", "Variant_ID", "dbo.Variants");
            DropForeignKey("dbo.Customers", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Penjualans", "Customer_Id", "dbo.Customers");
            DropForeignKey("dbo.Customers", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Customers", "CreatedBy_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Customers", new[] { "Variant_ID" });
            DropIndex("dbo.Customers", new[] { "UpdatedBy_Id" });
            DropIndex("dbo.Customers", new[] { "DeletedBy_Id" });
            DropIndex("dbo.Customers", new[] { "CreatedBy_Id" });
            DropIndex("dbo.Customers", new[] { "NoPol" });
            DropIndex("dbo.Penjualans", new[] { "Customer_Id" });
            DropColumn("dbo.Penjualans", "Customer_Id");
            DropTable("dbo.Customers");
        }
    }
}
