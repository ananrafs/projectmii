namespace Bengkel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dummyApi : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DummyApis",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DummyApis");
        }
    }
}
