namespace Bengkel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class aaa : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Customers", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Customers", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Customers", "Variant_ID", "dbo.Variants");
            DropForeignKey("dbo.Customers", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Customers", new[] { "CreatedBy_Id" });
            DropIndex("dbo.Customers", new[] { "DeletedBy_Id" });
            DropIndex("dbo.Customers", new[] { "Variant_ID" });
            DropIndex("dbo.Customers", new[] { "UpdatedBy_Id" });
            
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Kunci = c.Int(nullable: false, identity: true),
                        Owner = c.String(),
                        Tahun = c.Int(nullable: false),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        Deleted = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        Variant_ID = c.Int(),
                        UpdatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Kunci);
            
            AddColumn("dbo.Penjualans", "Customer_Kunci", c => c.Int());
            CreateIndex("dbo.Penjualans", "Customer_Kunci");
            CreateIndex("dbo.Customers", "UpdatedBy_Id");
            CreateIndex("dbo.Customers", "Variant_ID");
            CreateIndex("dbo.Customers", "DeletedBy_Id");
            CreateIndex("dbo.Customers", "CreatedBy_Id");
            AddForeignKey("dbo.Customers", "UpdatedBy_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Customers", "Variant_ID", "dbo.Variants", "ID");
            AddForeignKey("dbo.Penjualans", "Customer_Kunci", "dbo.Customers", "Kunci");
            AddForeignKey("dbo.Customers", "DeletedBy_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Customers", "CreatedBy_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
