namespace Bengkel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class second : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Factories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        Deleted = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        UpdatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DeletedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy_Id)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.DeletedBy_Id)
                .Index(t => t.UpdatedBy_Id);
            
            CreateTable(
                "dbo.Variants",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        Deleted = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        Factory_ID = c.Int(),
                        UpdatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DeletedBy_Id)
                .ForeignKey("dbo.Factories", t => t.Factory_ID)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy_Id)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.DeletedBy_Id)
                .Index(t => t.Factory_ID)
                .Index(t => t.UpdatedBy_Id);
            
            CreateTable(
                "dbo.Parts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Price = c.Int(nullable: false),
                        Stock = c.Int(nullable: false),
                        Kind = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        Deleted = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        UpdatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DeletedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy_Id)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.DeletedBy_Id)
                .Index(t => t.UpdatedBy_Id);
            
            CreateTable(
                "dbo.PartVariants",
                c => new
                    {
                        Part_ID = c.Int(nullable: false),
                        Variant_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Part_ID, t.Variant_ID })
                .ForeignKey("dbo.Parts", t => t.Part_ID, cascadeDelete: true)
                .ForeignKey("dbo.Variants", t => t.Variant_ID, cascadeDelete: true)
                .Index(t => t.Part_ID)
                .Index(t => t.Variant_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Variants", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.PartVariants", "Variant_ID", "dbo.Variants");
            DropForeignKey("dbo.PartVariants", "Part_ID", "dbo.Parts");
            DropForeignKey("dbo.Parts", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Parts", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Parts", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Variants", "Factory_ID", "dbo.Factories");
            DropForeignKey("dbo.Variants", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Variants", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Factories", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Factories", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Factories", "CreatedBy_Id", "dbo.AspNetUsers");
            DropIndex("dbo.PartVariants", new[] { "Variant_ID" });
            DropIndex("dbo.PartVariants", new[] { "Part_ID" });
            DropIndex("dbo.Parts", new[] { "UpdatedBy_Id" });
            DropIndex("dbo.Parts", new[] { "DeletedBy_Id" });
            DropIndex("dbo.Parts", new[] { "CreatedBy_Id" });
            DropIndex("dbo.Variants", new[] { "UpdatedBy_Id" });
            DropIndex("dbo.Variants", new[] { "Factory_ID" });
            DropIndex("dbo.Variants", new[] { "DeletedBy_Id" });
            DropIndex("dbo.Variants", new[] { "CreatedBy_Id" });
            DropIndex("dbo.Factories", new[] { "UpdatedBy_Id" });
            DropIndex("dbo.Factories", new[] { "DeletedBy_Id" });
            DropIndex("dbo.Factories", new[] { "CreatedBy_Id" });
            DropTable("dbo.PartVariants");
            DropTable("dbo.Parts");
            DropTable("dbo.Variants");
            DropTable("dbo.Factories");
        }
    }
}
