namespace Bengkel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pindahEnumProp : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Variants", "Kind", c => c.Int(nullable: false));
            AddColumn("dbo.Variants", "Type", c => c.Int(nullable: false));
            DropColumn("dbo.Parts", "Kind");
            DropColumn("dbo.Parts", "Type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Parts", "Type", c => c.Int(nullable: false));
            AddColumn("dbo.Parts", "Kind", c => c.Int(nullable: false));
            DropColumn("dbo.Variants", "Type");
            DropColumn("dbo.Variants", "Kind");
        }
    }
}
