using Bengkel.Models;
using Bengkel.Util;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Bengkel.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Bengkel.Util.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Bengkel.Util.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

            var roleAdmin= roleManager.FindByName("Admin");
            if (roleAdmin == null)
            {
                roleManager.Create(new IdentityRole { Name = "Admin" });
            }
            var roleClerk = roleManager.FindByName("Clerk");
            if (roleClerk == null)
            {
                roleManager.Create(new IdentityRole { Name = "Clerk" });
            }

            var roleCustomer = roleManager.FindByName("Customer");
            if (roleCustomer == null)
            {
                roleManager.Create(new IdentityRole { Name = "Customer" });
            }

            var roleMechanic = roleManager.FindByName("Mechanic");
            if (roleMechanic == null)
            {
                roleManager.Create(new IdentityRole { Name = "Mechanic" });
            }


            var author = new ApplicationUser
            {
                PhoneNumber = "+6282311028592",
                PhoneNumberConfirmed = true,
                UserName = "admin",
                Email = "author2@training.id"
            };
            if (manager.FindByName("admin") == null)
            {
                manager.Create(author, "Training@2018");
                manager.AddToRoles(author.Id, new string[] { "Admin" });
            }
        }
    }
}
