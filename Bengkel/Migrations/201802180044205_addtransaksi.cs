namespace Bengkel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtransaksi : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.PartVariants", newName: "VariantParts");
            DropPrimaryKey("dbo.VariantParts");
            CreateTable(
                "dbo.DetailBelis",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        jmlBeli = c.Int(nullable: false),
                        Part_ID = c.Int(),
                        Pembelian_Id = c.Int(),
                        Supplier_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Parts", t => t.Part_ID)
                .ForeignKey("dbo.Pembelians", t => t.Pembelian_Id)
                .ForeignKey("dbo.Suppliers", t => t.Supplier_Id)
                .Index(t => t.Part_ID)
                .Index(t => t.Pembelian_Id)
                .Index(t => t.Supplier_Id);
            
            CreateTable(
                "dbo.DetailJuals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        JmlJual = c.Int(nullable: false),
                        Part_ID = c.Int(),
                        Penjualan_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Parts", t => t.Part_ID)
                .ForeignKey("dbo.Penjualans", t => t.Penjualan_Id)
                .Index(t => t.Part_ID)
                .Index(t => t.Penjualan_Id);
            
            CreateTable(
                "dbo.Penjualans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TglJual = c.DateTimeOffset(precision: 7),
                        Customer_NoPol = c.String(maxLength: 128),
                        ServiceBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.Customer_NoPol)
                .ForeignKey("dbo.AspNetUsers", t => t.ServiceBy_Id)
                .Index(t => t.Customer_NoPol)
                .Index(t => t.ServiceBy_Id);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        NoPol = c.String(nullable: false, maxLength: 128),
                        Owner = c.String(),
                        Tahun = c.Int(nullable: false),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        Deleted = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        UpdatedBy_Id = c.String(maxLength: 128),
                        Variant_ID = c.Int(),
                    })
                .PrimaryKey(t => t.NoPol)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DeletedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy_Id)
                .ForeignKey("dbo.Variants", t => t.Variant_ID)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.DeletedBy_Id)
                .Index(t => t.UpdatedBy_Id)
                .Index(t => t.Variant_ID);
            
            CreateTable(
                "dbo.Pembelians",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        tglBeli = c.DateTimeOffset(precision: 7),
                        Reciever_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Reciever_Id)
                .Index(t => t.Reciever_Id);
            
            CreateTable(
                "dbo.Suppliers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        Created = c.DateTimeOffset(precision: 7),
                        Updated = c.DateTimeOffset(precision: 7),
                        Deleted = c.DateTimeOffset(precision: 7),
                        CreatedBy_Id = c.String(maxLength: 128),
                        DeletedBy_Id = c.String(maxLength: 128),
                        UpdatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.DeletedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy_Id)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.DeletedBy_Id)
                .Index(t => t.UpdatedBy_Id);
            
            AddPrimaryKey("dbo.VariantParts", new[] { "Variant_ID", "Part_ID" });
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Suppliers", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.DetailBelis", "Supplier_Id", "dbo.Suppliers");
            DropForeignKey("dbo.Suppliers", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Suppliers", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Pembelians", "Reciever_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.DetailBelis", "Pembelian_Id", "dbo.Pembelians");
            DropForeignKey("dbo.Penjualans", "ServiceBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.DetailJuals", "Penjualan_Id", "dbo.Penjualans");
            DropForeignKey("dbo.Customers", "Variant_ID", "dbo.Variants");
            DropForeignKey("dbo.Customers", "UpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Penjualans", "Customer_NoPol", "dbo.Customers");
            DropForeignKey("dbo.Customers", "DeletedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Customers", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.DetailJuals", "Part_ID", "dbo.Parts");
            DropForeignKey("dbo.DetailBelis", "Part_ID", "dbo.Parts");
            DropIndex("dbo.Suppliers", new[] { "UpdatedBy_Id" });
            DropIndex("dbo.Suppliers", new[] { "DeletedBy_Id" });
            DropIndex("dbo.Suppliers", new[] { "CreatedBy_Id" });
            DropIndex("dbo.Pembelians", new[] { "Reciever_Id" });
            DropIndex("dbo.Customers", new[] { "Variant_ID" });
            DropIndex("dbo.Customers", new[] { "UpdatedBy_Id" });
            DropIndex("dbo.Customers", new[] { "DeletedBy_Id" });
            DropIndex("dbo.Customers", new[] { "CreatedBy_Id" });
            DropIndex("dbo.Penjualans", new[] { "ServiceBy_Id" });
            DropIndex("dbo.Penjualans", new[] { "Customer_NoPol" });
            DropIndex("dbo.DetailJuals", new[] { "Penjualan_Id" });
            DropIndex("dbo.DetailJuals", new[] { "Part_ID" });
            DropIndex("dbo.DetailBelis", new[] { "Supplier_Id" });
            DropIndex("dbo.DetailBelis", new[] { "Pembelian_Id" });
            DropIndex("dbo.DetailBelis", new[] { "Part_ID" });
            DropPrimaryKey("dbo.VariantParts");
            DropTable("dbo.Suppliers");
            DropTable("dbo.Pembelians");
            DropTable("dbo.Customers");
            DropTable("dbo.Penjualans");
            DropTable("dbo.DetailJuals");
            DropTable("dbo.DetailBelis");
            AddPrimaryKey("dbo.VariantParts", new[] { "Part_ID", "Variant_ID" });
            RenameTable(name: "dbo.VariantParts", newName: "PartVariants");
        }
    }
}
