namespace Bengkel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class delKind : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Variants", "Kind");
            DropColumn("dbo.Variants", "Type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Variants", "Type", c => c.Int(nullable: false));
            AddColumn("dbo.Variants", "Kind", c => c.Int(nullable: false));
        }
    }
}
