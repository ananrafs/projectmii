﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bengkel.ViewModel.api
{
    public class ApiPenjualanVM
    {

    }

    public class ApiDetailJualVM
    {
        public int Id { get; set; }
        public int Part { get; set; }
        public int JmlJual { get; set; }
    }

    public class GeneralRespPenjualan
    {
        public int IdCust { get; set; }
        public List<ApiDetailJualVM> datas { get; set; }
    }
}