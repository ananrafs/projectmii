﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bengkel.Util;

namespace Bengkel.ViewModel.api
{
    public class ApiSupplierVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool isDel { get; set; }
        public DateTimeOffset? Created { get; set; }
        public DateTimeOffset? Updated { get; set; }
        public string CreatedBy { get; set; }
        public string UpdateBy { get; set; }
    }

    public class ApiSupplieryDR : DataTableResult
    {
        public List<ApiSupplierVM> data { get; set; }
    }

    public class ApiSupplierDropDown
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}