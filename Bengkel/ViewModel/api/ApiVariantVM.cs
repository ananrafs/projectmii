﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bengkel.Models.product;
using Bengkel.Util;

namespace Bengkel.ViewModel.api
{
    public class ApiVariantVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool isDel { get; set; }
        public DateTimeOffset? Created { get; set; }
        public DateTimeOffset? Updated { get; set; }
        public string CreatedBy { get; set; }
        public string UpdateBy { get; set; }

        public Factory Pabrikan { get; set; }
    }

    public class ApiVariantDR : DataTableResult
    {
        public List<ApiVariantVM> data { get; set; }
    }

    public class ApiVariantDropDown
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}