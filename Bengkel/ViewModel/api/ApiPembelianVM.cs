﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bengkel.ViewModel.api
{
    public class ApiPembelianVM
    {
    }

    public class ApiDetailBeliVM
    {
        public int Id { get; set; }
        public int jmlBeli { get; set; }
        public int PartId { get; set; }
        public int PembelianId { get; set; }
        public int SupplierId { get; set; }
    }

    public class PushDetailDataVM
    {
        public int count { get; set; }
        public List<ApiDetailBeliVM> data { get; set; }
    }
}