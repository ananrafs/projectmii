﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bengkel.Util;

namespace Bengkel.ViewModel.api
{
    public class ApiGetFactoryVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool isDel { get; set; }
        public DateTimeOffset? Created { get; set; }
        public DateTimeOffset? Updated { get; set; }
        public string CreatedBy { get; set; }
        public string UpdateBy { get; set; }
    }

    public class ApiGetFactoryDR : DataTableResult
    {
        public List<ApiGetFactoryVM> data { get; set; }
    }


    public class ApiGetFactoryVMDropDown
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}