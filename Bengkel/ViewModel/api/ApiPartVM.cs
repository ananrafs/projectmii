﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bengkel.Models.product;
using Bengkel.Util;

namespace Bengkel.ViewModel.api
{
    public class ApiPartVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Stock { get; set; }
        public int Price { get; set; }
        public bool isDel { get; set; }
        public DateTimeOffset? Created { get; set; }
        public DateTimeOffset? Updated { get; set; }
        public string CreatedBy { get; set; }
        public string UpdateBy { get; set; }

        public ICollection<ApiVariantVM> Variants { get; set; }
    }

    public class ApiPartTransaksiVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Stock { get; set; }
        public int Price { get; set; }
        public int SellPrice { get; set; }

        public ICollection<ApiVariantVM> ApiVariantVms { get; set; }
    }




    public class ApiPartDR : DataTableResult
    {
        public List<ApiPartVM> data { get; set; }
    }

    public class ApiPartDropDown
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}