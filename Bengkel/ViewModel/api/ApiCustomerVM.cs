﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bengkel.Models.product;
using Bengkel.Util;

namespace Bengkel.ViewModel.api
{
    public class ApiCustomerVM
    {
        public int Id { get; set; }
        public string NoPol { get; set; }
        public string Owner { get; set; }
        public int Tahun { get; set; }
        public DateTimeOffset? Created { get; set; }
        public DateTimeOffset? Updated { get; set; }
        public string CreatedBy { get; set; }
        public string UpdateBy { get; set; }
        public Variant Variant { get; set; }
    }

    public class ApiCustDR : DataTableResult
    {
        public List<ApiCustomerVM> data { get; set; }
    }

    public class ApiCustDropDown
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}