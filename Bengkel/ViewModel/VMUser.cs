﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bengkel.ViewModel
{
    public class VMUser
    {
        public string Id { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public List<string> Roles {get; set;}
    }
}