﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bengkel.Models.product;
using Bengkel.Models.util;
using Type = Bengkel.Models.util.Type;

namespace Bengkel.ViewModel.Clerk
{
    public class VMVariant
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTimeOffset? Created { get; set; }
        public DateTimeOffset? Updated { get; set; }
        public Factory Factory { get; set; }
        public Kind Kind { get; set; }
        public Type Type { get; set; }

    }
}