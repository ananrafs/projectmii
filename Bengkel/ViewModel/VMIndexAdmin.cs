﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bengkel.Models;

namespace Bengkel.ViewModel
{
    public class VMIndexAdmin
    {
        public string Id { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public List<VMUser> Users { get; set; }
    }
}