﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Bengkel.Models.product;

namespace Bengkel.Models.transaksi
{
    public class DetailJual
    {
        [Key]
        public int Id { get; set; }

        public Penjualan Penjualan { get; set; }
        public Part Part { get; set; }
        public int JmlJual { get; set; }
    }
}