﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bengkel.Models.transaksi
{
    public class Pembelian
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTimeOffset? tglBeli { get; set; }
        public virtual ApplicationUser Reciever { get; set; }
        public virtual ICollection<DetailBeli> DetailBelis { get; set; }
    }
}