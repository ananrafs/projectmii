﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Bengkel.Models.mitra;

namespace Bengkel.Models.transaksi
{
    public class Penjualan
    {
        [Key]
        public int Id { get; set; }

        public DateTimeOffset? TglJual { get; set; }

        public ApplicationUser ServiceBy { get; set; }

        public virtual ICollection<DetailJual> DetailJuals { get; set; }

        public Customer Customer { get; set; }
    }
}