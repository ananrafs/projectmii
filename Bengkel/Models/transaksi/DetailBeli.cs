﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Bengkel.Models.mitra;
using Bengkel.Models.product;

namespace Bengkel.Models.transaksi
{
    public class DetailBeli
    {
        [Key]
        public int Id { get; set; }
        public Part Part { get; set; }
        public Pembelian Pembelian { get; set; }
        public Supplier Supplier { get; set; }
        public int jmlBeli { get; set; }
    }
}