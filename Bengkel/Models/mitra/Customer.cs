﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Bengkel.Models.product;
using Bengkel.Models.transaksi;
using Bengkel.Models.util;

namespace Bengkel.Models.mitra
{
    public class Customer : GeneralMeta
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(25)]
        [Index(IsUnique = true)]
        public string NoPol { get; set; }
        public int Tahun { get; set; }
        public string Owner { get; set; }

        public Variant Variant { get; set; }
        public ICollection<Penjualan> Penjualans { get; set; }


    }
}