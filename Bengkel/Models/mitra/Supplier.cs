﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Bengkel.Models.transaksi;
using Bengkel.Models.util;

namespace Bengkel.Models.mitra
{
    public class Supplier : BaseProp
    {
        [Key]
        public int Id { get; set; }

        public virtual ICollection<DetailBeli> DetailBelis { get; set; }
    }
}