﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bengkel.Models.util
{
    public class GeneralMeta
    {
        public bool IsDeleted { get; set; } = false;
        public DateTimeOffset? Created { get; set; } = DateTimeOffset.Now;
        public DateTimeOffset? Updated { get; set; } =  DateTimeOffset.Now;
        public DateTimeOffset? Deleted { get; set; }
        public virtual ApplicationUser CreatedBy { get; set; }
        public virtual ApplicationUser UpdatedBy { get; set; }
        public virtual ApplicationUser DeletedBy { get; set; }
    }
}