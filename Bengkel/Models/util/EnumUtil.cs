﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bengkel.Models.util
{
    public enum Type
    {
        Matic,
        Coupling
    }

    public enum Kind
    {
        Car,
        MotorCycle
    }
}