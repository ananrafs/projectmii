﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Bengkel.Models.util;

namespace Bengkel.Models.product
{
    public class Factory : BaseProp
    {
        [Key]
        public int ID { get; set; }

        public virtual ICollection<Variant> Variants { get; set; }
    }
}