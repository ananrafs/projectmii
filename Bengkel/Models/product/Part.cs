﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Bengkel.Models.transaksi;
using Bengkel.Models.util;
using Type = Bengkel.Models.util.Type;

namespace Bengkel.Models.product
{
    public class Part : BaseProp
    {
        [Key]
        public int ID { get; set; }
        public int Price { get; set; }
        public int Stock { get; set; }
        public ICollection<Variant> Variants { get; set; }

        public virtual ICollection<DetailBeli> DetailBelis { get; set; }
        public virtual ICollection<DetailJual> DetailJuals { get; set; }

        
    }
}