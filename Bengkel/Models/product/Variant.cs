﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Bengkel.Models.mitra;
using Bengkel.Models.util;
using Type = Bengkel.Models.util.Type;

namespace Bengkel.Models.product
{
    public class Variant : BaseProp
    {
        [Key]
        public int ID { get; set; }

        [NotMapped]
        public int idFactory { get; set; }

        public virtual Factory Factory { get; set; }

        public ICollection<Part> Parts { get; set; }

        public ICollection<Customer> Customers { get; set; }

        

    }
}