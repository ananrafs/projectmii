﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Bengkel.Startup))]
namespace Bengkel
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
