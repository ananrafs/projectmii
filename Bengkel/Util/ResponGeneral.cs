﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bengkel.Util
{
    public class ResponGeneral
    {
        public bool status { get; set; }
        public string msg { get; set; }
        public object data { get; set; }
        public List<object> datas { get; set; }
    }
}