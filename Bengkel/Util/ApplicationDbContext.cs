﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Bengkel.Models;
using Bengkel.Models.mitra;
using Bengkel.Models.product;
using Bengkel.Models.transaksi;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Bengkel.Util
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Factory> Factories { get; set; }

        public DbSet<Part> Parts { get; set; }

        public DbSet<Variant> Variants { get; set; }

        public DbSet<DummyApi> DummyApis { get; set; }

        public DbSet<Supplier> Suppliers { get; set; }

        public DbSet<Pembelian> Pembelians { get; set; }

        public DbSet<DetailBeli> DetailBelis { get; set; }

        
        public DbSet<Penjualan> Penjualans { get; set; }
        public DbSet<DetailJual> DetailJuals { get; set; }

        public System.Data.Entity.DbSet<Bengkel.Models.mitra.Customer> Customers { get; set; }
    }
}