﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Bengkel.Models;
using Bengkel.Models.product;
using Bengkel.Util;
using Bengkel.ViewModel.Clerk;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Bengkel.Controllers.clerk
{
    [Authorize(Roles = "Clerk")]
    public class ClerkController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private UserManager<ApplicationUser> manager = new UserManager<ApplicationUser>
            (new UserStore<ApplicationUser>(new ApplicationDbContext()));
        private RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>
            (new RoleStore<IdentityRole>(new ApplicationDbContext()));


        // GET: Clerk
        public async Task<ActionResult> Index()
        {
            var currentUser = await db.Users.Where(x => x.UserName == User.Identity.Name)
                .SingleOrDefaultAsync();
            ViewBag.curUser = currentUser;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(VMFactory factory)
        {
            if (ModelState.IsValid)
            {
                var currentUser = await db.Users.Where(x => x.UserName == User.Identity.Name)
                    .SingleOrDefaultAsync();
                var currTime = DateTimeOffset.UtcNow;
                var pushFac = new Factory();
                pushFac.Name = factory.Name;
                pushFac.IsDeleted = false;
                pushFac.Created = currTime;
                pushFac.Updated = currTime;
                pushFac.CreatedBy = currentUser;
                pushFac.UpdatedBy = currentUser;


            }
            return View(factory);
        }

        public async Task<ActionResult> LoadFactory()
        {
             var currentUser = await db.Users.Where(x => x.UserName == User.Identity.Name)
                .SingleOrDefaultAsync();
            return PartialView("clerk/_PartialFactory");
        }

        public async Task<ActionResult> loadVariantPage()
        {
            return PartialView("clerk/_PartialVarian");
        }

        public async Task<ActionResult> loadPartPage()
        {
            return PartialView("clerk/_PartialPart");
        }

        public async Task<ActionResult> loadSupplier()
        {
            return PartialView("clerk/_PartialSupplier");
        }

        public async Task<ActionResult> loadCustomer()
        {
            return PartialView("clerk/_PartialCustomer");
        }

        public async Task<ActionResult> loadPembelian()
        {
            return PartialView("clerk/_PartialPembelian");
        }


        public async Task<ActionResult> loadPenjualan()
        {
            //return PartialView("clerk/_PartialPenjualan");
            return File("~/Views/Shared/clerk/AgularTest.html", "text/html");
        }
    }
}