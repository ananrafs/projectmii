﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Bengkel.Models;
using Bengkel.Util;
using Bengkel.ViewModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace Bengkel.Controllers.admin
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private UserManager<ApplicationUser> manager = new UserManager<ApplicationUser>
            (new UserStore<ApplicationUser>(new ApplicationDbContext()));
        private RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>
            (new RoleStore<IdentityRole>(new ApplicationDbContext()));

        // GET: Admin
        public async Task<ActionResult> Index()
        {
            var currentUser = await db.Users.Where(x => x.UserName == User.Identity.Name)
                .SingleOrDefaultAsync();
            IEnumerable<ApplicationUser> users = await manager.Users.ToListAsync();
            List<IdentityRole> roles = await roleManager.Roles.ToListAsync();
            var vmIndexAdmin = new VMIndexAdmin();
            vmIndexAdmin.Id = currentUser.Id;
            vmIndexAdmin.Username = currentUser.UserName;
            vmIndexAdmin.Email = currentUser.Email;
            vmIndexAdmin.Phone = currentUser.PhoneNumber;
            vmIndexAdmin.Users = new List<VMUser>();
            foreach (var applicationUser in users)
            {
                var vmUser = new VMUser();
                vmUser.Id = applicationUser.Id;
                vmUser.Username = applicationUser.UserName;
                vmUser.Email = applicationUser.Email;
                vmUser.Phone = applicationUser.PhoneNumber;
                vmUser.Roles = new List<string>();
                foreach (var role in applicationUser.Roles)
                {
                    string job = roles.Find(identityRole => identityRole.Id == role.RoleId).Name;
                    vmUser.Roles.Add(job);
                }
                vmIndexAdmin.Users.Add(vmUser);
            }

            return View(vmIndexAdmin);
        }

        // GET: Admin/Details/5
        public async Task<ActionResult> Details(string id)
        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            ApplicationUser applicationUser = await db.ApplicationUsers.FindAsync(id);
//            if (applicationUser == null)
//            {
//                return HttpNotFound();
//            }
//            return View(applicationUser);
            return View();
        }

        // GET: Admin/Create
        public ActionResult Create()
        {
            ViewBag.Name = new SelectList(db.Roles.Where(u => !u.Name.Contains("Admin"))
                .ToList(), "Name", "Name");
            return View();
        }

        // POST: Admin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(RegisterViewModel model)
        {

            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.UserName,
                    Email = model.Email,
                    PhoneNumber = model.Phone,

                };
                var result = await manager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {

                    //  await this.manager.AddToRoleAsync(user.Id, model.UserRoles
                    string role = model.UserRoles;
                    await this.manager.AddToRoleAsync(user.Id, role);

                    //Ends Here 
                    return RedirectToAction("Index", "Admin");
                }
                AddErrors(result);
            }

            ViewBag.Name = new SelectList(db.Roles.Where(u => !u.Name.Contains("Admin"))
                .ToList(), "Name", "Name");
            return View(model);
        }

        // GET: Admin/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            ApplicationUser applicationUser = await db.ApplicationUsers.FindAsync(id);
//            if (applicationUser == null)
//            {
//                return HttpNotFound();
//            }
//            return View(applicationUser);
            return View();
        }

        // POST: Admin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] ApplicationUser applicationUser)
        {
//            if (ModelState.IsValid)
//            {
//                db.Entry(applicationUser).State = EntityState.Modified;
//                await db.SaveChangesAsync();
//                return RedirectToAction("Index");
//            }
            return View(applicationUser);
        }

        // GET: Admin/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            ApplicationUser applicationUser = await db.ApplicationUsers.FindAsync(id);
//            if (applicationUser == null)
//            {
//                return HttpNotFound();
//            }
//            return View(applicationUser);
            return View();
        }

        // POST: Admin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
//            ApplicationUser applicationUser = await db.ApplicationUsers.FindAsync(id);
//            db.ApplicationUsers.Remove(applicationUser);
//            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}
