﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using System.Web.Mvc;
using Bengkel.Models;
using Bengkel.Models.product;
using Bengkel.Util;
using Bengkel.ViewModel.api;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Bengkel.Controllers.api.factory
{
    public class FactoriesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private UserManager<ApplicationUser> manager = new UserManager<ApplicationUser>
            (new UserStore<ApplicationUser>(new ApplicationDbContext()));
        private RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>
            (new RoleStore<IdentityRole>(new ApplicationDbContext()));

        // GET: api/Factories
        public JsonResult<List<ApiGetFactoryVM>> GetFactories()
        {
            var result = db.Factories.Where(factory => factory.IsDeleted == false).ToList();
            List<ApiGetFactoryVM> factoryVms = new List<ApiGetFactoryVM>();
            result.ForEach(factory =>
            {
                var item = new ApiGetFactoryVM();
                item.Id = factory.ID;
                item.Name = factory.Name;
                item.Created = factory.Created;
                item.Updated = factory.Updated;
                item.isDel = factory.IsDeleted;
                item.CreatedBy = factory.CreatedBy.UserName;
                item.UpdateBy = factory.UpdatedBy.UserName;
                factoryVms.Add(item);
            });
            return Json(factoryVms);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/getAllFactory")]
        public DataTableResponse GetProducts()
        {
            var result = db.Factories.Where(factory => factory.IsDeleted == false).ToList();
            List<ApiGetFactoryVM> factoryVms = new List<ApiGetFactoryVM>();
            result.ForEach(factory =>
            {
                var item = new ApiGetFactoryVM();
                item.Id = factory.ID;
                item.Name = factory.Name;
                item.Created = factory.Created;
                item.Updated = factory.Updated;
                item.isDel = factory.IsDeleted;
                item.CreatedBy = factory.CreatedBy.UserName;
                item.UpdateBy = factory.UpdatedBy.UserName;
                factoryVms.Add(item);
            });

            return new DataTableResponse
            {
                recordsTotal = factoryVms.Count(),
                recordsFiltered = 10,
                data = factoryVms.Take(10).ToArray()
            };
        }

        
        [System.Web.Http.Route("api/getFactoryDataTable")]
        public DataTableResponse getDataTable(DataTableRequest dr)
        {
            var products = db.Factories.Where(factory => factory.IsDeleted == false).ToList();
            List<ApiGetFactoryVM> factoryVms = new List<ApiGetFactoryVM>();
            products.ForEach(factory =>
            {
                var item = new ApiGetFactoryVM();
                item.Id = factory.ID;
                item.Name = factory.Name;
                item.Created = factory.Created;
                item.Updated = factory.Updated;
                item.isDel = factory.IsDeleted;
                if (factory.CreatedBy != null)
                {
                    item.CreatedBy = factory.CreatedBy.UserName;
                    item.UpdateBy = factory.UpdatedBy.UserName;
                }
                factoryVms.Add(item);
            });

            // var products = productRepository.GetProducts();

            // Searching Data
            IEnumerable<ApiGetFactoryVM> filteredProducts;
            if (dr.Search.Value != "" || dr.Search.Value != null)
            {
                var searchText = dr.Search.Value.Trim();

                filteredProducts = factoryVms.Where(p =>
                        p.Name.Contains(searchText));
            }
            else
            {
                filteredProducts = factoryVms;
            }

            // Sort Data
            if (dr.Order.Any())
            {
                int sortColumnIndex = dr.Order[0].Column;
                string sortDirection = dr.Order[0].Dir;

                Func<ApiGetFactoryVM, string> orderingFunctionString = null;
                Func<ApiGetFactoryVM, int> orderingFunctionInt = null;
                Func<ApiGetFactoryVM, decimal?> orderingFunctionDecimal = null;

                switch (sortColumnIndex)
                {
                    case 0:     // ProductID
                        {
                            orderingFunctionInt = (c => c.Id);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionInt)
                                    : filteredProducts.OrderByDescending(orderingFunctionInt);
                            break;
                        }
                    case 1:     // ProductName
                        {
                            orderingFunctionString = (c => c.Name);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionString)
                                    : filteredProducts.OrderByDescending(orderingFunctionString);
                            break;
                        }
                }
            }

            // Paging Data
            var pagedProducts = filteredProducts.Skip(dr.Start).Take(dr.Length);

            return new DataTableResponse
            {
                draw = dr.Draw,
                recordsTotal = factoryVms.Count(),
                recordsFiltered = factoryVms.Count(),
                data = pagedProducts.ToArray(),
                error = ""
            };
            
        }

        // GET: api/Factories/5
        [ResponseType(typeof(Factory))]
        public async Task<IHttpActionResult> GetFactory(int id)
        {
            Factory factory = await db.Factories.FindAsync(id);
            if (factory == null)
            {
                return NotFound();
            }

            return Ok(factory);
        }

        // PUT: api/Factories/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutFactory([FromUri]int id, [FromBody]ApiGetFactoryVM factoryVM)
        {
            var currentUser = await db.Users.Where(x => x.UserName == User.Identity.Name)
                .SingleOrDefaultAsync();
            Factory factory = await db.Factories.FindAsync(factoryVM.Id);
            if (factory == null)
            {
                return Json(new ResponGeneral()
                {
                    status = false,
                    msg = "entiti not found"
                });
            }

            factory.Name = factoryVM.Name;
            factory.Updated = DateTimeOffset.UtcNow;
            factory.UpdatedBy = currentUser;

            db.Entry(factory).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FactoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Json(new ResponGeneral()
            {
                status = true,
                msg = "Updated"
            }); 
        }

        // POST: api/Factories
        [ResponseType(typeof(Factory))]
        public async Task<IHttpActionResult> PostFactory(Factory factory)
        {
            var currentUser = await db.Users.Where(x => x.UserName == User.Identity.Name)
                .SingleOrDefaultAsync();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            factory.CreatedBy = currentUser;
            factory.UpdatedBy = currentUser;
            db.Factories.Add(factory);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = factory.ID }, factory);
        }

        // DELETE: api/Factories/5
        [ResponseType(typeof(Factory))]
        public async Task<IHttpActionResult> DeleteFactory(int id)
        {
            Factory factory = await db.Factories.FindAsync(id);
            if (factory == null)
            {
                return NotFound();
            }

            db.Factories.Remove(factory);
            await db.SaveChangesAsync();

            return Ok(factory);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FactoryExists(int id)
        {
            return db.Factories.Count(e => e.ID == id) > 0;
        }
    }
}