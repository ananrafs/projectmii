﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Bengkel.Models.product;
using Bengkel.Util;
using Bengkel.ViewModel.api;

namespace Bengkel.Controllers.api.product
{
    public class PartsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Parts
        public IQueryable<Part> GetParts()
        {
            return db.Parts;
        }

        // GET: api/Parts/5
        [ResponseType(typeof(Part))]
        public async Task<IHttpActionResult> GetPart(int id)
        {
            Part part = await db.Parts.FindAsync(id);
            if (part == null)
            {
                return NotFound();
            }

            return Ok(part);
        }

        // PUT: api/Parts/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPart(int id, Part part)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != part.ID)
            {
                return BadRequest();
            }

            db.Entry(part).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PartExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Parts
        [ResponseType(typeof(Part))]
        public async Task<ResponGeneral> PostPart(ApiPartVM part)
        {
            var currentUser = await db.Users.Where(x => x.UserName == User.Identity.Name)
                .SingleOrDefaultAsync();
            List<Variant> variants = new List<Variant>();
            foreach (var variant in part.Variants)
            {
                var varItem = await db.Variants.FindAsync(variant.Id);
                variants.Add(varItem);
            }

            var partData = new Part
            {
                Name = part.Name,
                Stock = int.Parse(part.Stock.ToString()),
                Price = int.Parse(part.Price.ToString()),
                CreatedBy = currentUser,
                UpdatedBy = currentUser,
                Variants = variants
            };

            db.Parts.Add(partData);

            var result = await db.SaveChangesAsync();
            if (result > 0)
            {
                return new ResponGeneral()
                {
                    status = true,
                    msg = "insert berhasil"
                };
            }
            else
            {
                return new ResponGeneral()
                {
                    status = false,
                    msg = "insert gagal"
                };
            }
        }

        // DELETE: api/Parts/5
        [ResponseType(typeof(Part))]
        public async Task<IHttpActionResult> DeletePart(int id)
        {
            Part part = await db.Parts.FindAsync(id);
            if (part == null)
            {
                return NotFound();
            }

            db.Parts.Remove(part);
            await db.SaveChangesAsync();

            return Ok(part);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PartExists(int id)
        {
            return db.Parts.Count(e => e.ID == id) > 0;
        }

        [Route("api/getVariantDropDown")]
        public async Task<IEnumerable<ApiVariantDropDown>> getVariantDropDown()
        {
            List<ApiVariantDropDown> lists = new List<ApiVariantDropDown>();
            var parts = await db.Variants.Where(v => v.IsDeleted == false).ToListAsync();
            parts.ForEach(v =>
            {
                var item = new ApiVariantDropDown();
                item.Id = v.ID;
                item.Name = v.Name;
                lists.Add(item);
            });
            return lists;
        }

        [System.Web.Http.Route("api/getPartDataTable")]
        public DataTableResponse getDataTable(DataTableRequest dr)
        {
            var products = db.Parts.Include("Variants").Where(v => v.IsDeleted == false).ToList();
            List<ApiPartVM> partsVm = new List<ApiPartVM>();
            products.ForEach(p =>
            {
                var item = new ApiPartVM();
                item.Id = p.ID;
                item.Name = p.Name;
                item.Created = p.Created;
                item.Updated = p.Updated;
                item.isDel = p.IsDeleted;
                item.CreatedBy = p.CreatedBy.UserName;
                item.UpdateBy = p.UpdatedBy.UserName;
                item.Stock = p.Stock;
                item.Price = p.Price;
                var variant = new List<ApiVariantVM>();
                foreach (var objVariant in p.Variants)
                {
                    var itemVariant = new ApiVariantVM()
                    {
                        Id = objVariant.ID,
                        Name = objVariant.Name,
                        isDel = objVariant.IsDeleted,
                        Created = objVariant.Created,
                        Updated = objVariant.Updated,
                        CreatedBy = objVariant.CreatedBy.UserName,
                        UpdateBy = objVariant.UpdatedBy.UserName,
                        Pabrikan = objVariant.Factory
                    };
                    variant.Add(itemVariant);
                    
                }
                item.Variants = variant;
                partsVm.Add(item);
            });

            // var products = productRepository.GetProducts();

            // Searching Data
            IEnumerable<ApiPartVM> filteredProducts;
            if (dr.Search.Value != "" || dr.Search.Value != null)
            {
                var searchText = dr.Search.Value.Trim();

                filteredProducts = partsVm.Where(p =>
                        p.Name.Contains(searchText));
            }
            else
            {
                filteredProducts = partsVm;
            }

            // Sort Data
            if (dr.Order.Any())
            {
                int sortColumnIndex = dr.Order[0].Column;
                string sortDirection = dr.Order[0].Dir;

                Func<ApiPartVM, string> orderingFunctionString = null;
                Func<ApiPartVM, int> orderingFunctionInt = null;
                Func<ApiPartVM, decimal?> orderingFunctionDecimal = null;

                switch (sortColumnIndex)
                {
                    case 0:     // ProductID
                        {
                            orderingFunctionInt = (c => c.Id);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionInt)
                                    : filteredProducts.OrderByDescending(orderingFunctionInt);
                            break;
                        }
                    case 1:     // ProductName
                        {
                            orderingFunctionString = (c => c.Name);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionString)
                                    : filteredProducts.OrderByDescending(orderingFunctionString);
                            break;
                        }
                }
            }

            // Paging Data
            var pagedProducts = filteredProducts.Skip(dr.Start).Take(dr.Length);

            return new DataTableResponse
            {
                draw = dr.Draw,
                recordsTotal = partsVm.Count(),
                recordsFiltered = partsVm.Count(),
                data = pagedProducts.ToArray(),
                error = ""
            };

        }

        [System.Web.Http.Route("api/getPenjualanDT")]
        public DataTableResponse getDataPembelian(DataTableRequest dr)
        {
            var products = db.Parts.Include("Variants").Where(v => v.IsDeleted == false).ToList();
            List<ApiPartTransaksiVM> partsVm = new List<ApiPartTransaksiVM>();
            products.ForEach(p =>
            {
                var item = new ApiPartTransaksiVM();
                item.Id = p.ID;
                item.Name = p.Name;
                item.Price = p.Price;
                item.Stock = p.Stock;
                item.SellPrice = p.Price + (p.Price / 100 * 15);
                var variant = new List<ApiVariantVM>();
                foreach (var objVariant in p.Variants)
                {
                    var itemVariant = new ApiVariantVM()
                    {
                        Id = objVariant.ID,
                        Name = objVariant.Name,
                        isDel = objVariant.IsDeleted,
                        Created = objVariant.Created,
                        Updated = objVariant.Updated,
                        CreatedBy = objVariant.CreatedBy.UserName,
                        UpdateBy = objVariant.UpdatedBy.UserName,
                        Pabrikan = objVariant.Factory
                    };
                    variant.Add(itemVariant);

                }
                item.ApiVariantVms = variant;
                partsVm.Add(item);
            });

            // var products = productRepository.GetProducts();

            // Searching Data
            IEnumerable<ApiPartTransaksiVM> filteredProducts;
            if (dr.Search.Value != "" || dr.Search.Value != null)
            {
                var searchText = dr.Search.Value.Trim();

                filteredProducts = partsVm.Where(p =>
                        p.Name.Contains(searchText));
            }
            else
            {
                filteredProducts = partsVm;
            }

            // Sort Data
            if (dr.Order.Any())
            {
                int sortColumnIndex = dr.Order[0].Column;
                string sortDirection = dr.Order[0].Dir;

                Func<ApiPartTransaksiVM, string> orderingFunctionString = null;
                Func<ApiPartTransaksiVM, int> orderingFunctionInt = null;
                Func<ApiPartTransaksiVM, decimal?> orderingFunctionDecimal = null;

                switch (sortColumnIndex)
                {
                    case 0:     // ProductID
                        {
                            orderingFunctionInt = (c => c.Id);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionInt)
                                    : filteredProducts.OrderByDescending(orderingFunctionInt);
                            break;
                        }
                    case 1:     // ProductName
                        {
                            orderingFunctionString = (c => c.Name);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionString)
                                    : filteredProducts.OrderByDescending(orderingFunctionString);
                            break;
                        }
                }
            }

            // Paging Data
            var pagedProducts = filteredProducts.Skip(dr.Start).Take(dr.Length);

            return new DataTableResponse
            {
                draw = dr.Draw,
                recordsTotal = partsVm.Count(),
                recordsFiltered = partsVm.Count(),
                data = pagedProducts.ToArray(),
                error = ""
            };

        }
    }
}