﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Bengkel.Models.product;
using Bengkel.Util;
using Bengkel.ViewModel.api;

namespace Bengkel.Controllers.api.factory
{
    public class VariantsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Variants
        public IQueryable<Variant> GetVariants()
        {
            return db.Variants;
        }

        // GET: api/Variants/5
        [ResponseType(typeof(Variant))]
        public async Task<IHttpActionResult> GetVariant(int id)
        {
            Variant variant = await db.Variants.FindAsync(id);
            if (variant == null)
            {
                return NotFound();
            }

            return Ok(variant);
        }

        // PUT: api/Variants/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutVariant([FromUri]int id, [FromBody]ApiVariantVM variantVM)
        {
            var currentUser = await db.Users.Where(x => x.UserName == User.Identity.Name)
                .SingleOrDefaultAsync();
            Variant variant = await db.Variants.FindAsync(variantVM.Id);
            Factory fact = await db.Factories.FindAsync(variantVM.Pabrikan.ID);

            if (variant == null)
            {
                return Json(new ResponGeneral()
                {
                    status = false,
                    msg = "entiti not found"
                });
            }

            variant.Name = variantVM.Name;
            variant.Updated = DateTimeOffset.UtcNow;
            variant.UpdatedBy = currentUser;
            variant.Factory = fact;

            db.Entry(variant).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VariantExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Json(new ResponGeneral()
            {
                status = true,
                msg = "Updated"
            });
        }

        // POST: api/Variants
        [ResponseType(typeof(Variant))]
        public async Task<ResponGeneral> PostVariant(Variant variant)
        {
            var currentUser = await db.Users.Where(x => x.UserName == User.Identity.Name)
                .SingleOrDefaultAsync();
            var fact = await db.Factories.FindAsync(variant.idFactory);
            
            variant.CreatedBy = currentUser;
            variant.UpdatedBy = currentUser;
            variant.Factory = fact;

            db.Variants.Add(variant);

            var result = await db.SaveChangesAsync();
            if (result > 0)
            {
                return new ResponGeneral()
                {
                    status = true,
                    msg = "insert berhasil"
                };
            }
            else
            {
                return new ResponGeneral()
                {
                    status = false,
                    msg = "insert gagal"
                };
            }

//            return CreatedAtRoute("DefaultApi", new { id = variant.ID }, variant);
        }

        // DELETE: api/Variants/5
        [ResponseType(typeof(Variant))]
        public async Task<IHttpActionResult> DeleteVariant(int id)
        {
            Variant variant = await db.Variants.FindAsync(id);
            if (variant == null)
            {
                return NotFound();
            }

            db.Variants.Remove(variant);
            await db.SaveChangesAsync();

            return Ok(variant);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VariantExists(int id)
        {
            return db.Variants.Count(e => e.ID == id) > 0;
        }

        [Route("api/getFactDropdown")]
        public async Task<IEnumerable<ApiGetFactoryVMDropDown>> getFactoryDropDown()
        {
            List<ApiGetFactoryVMDropDown> lists = new List<ApiGetFactoryVMDropDown>();
            var facts = await db.Factories.Where(factory => factory.IsDeleted == false).ToListAsync();
            facts.ForEach(factory =>
            {
                var item = new ApiGetFactoryVMDropDown();
                item.Id = factory.ID;
                item.Name = factory.Name;
                lists.Add(item);
            });
            return lists;
        }

        [System.Web.Http.Route("api/getVariantDataTable")]
        public DataTableResponse getDataTable(DataTableRequest dr)
        {
            var products = db.Variants.Include("Factory").Where(v => v.IsDeleted == false).ToList();
            List<ApiVariantVM> variantVm = new List<ApiVariantVM>();
            products.ForEach(v =>
            {
                var item = new ApiVariantVM();
                item.Id = v.ID;
                item.Name = v.Name;
                item.Created = v.Created;
                item.Updated = v.Updated;
                item.isDel = v.IsDeleted;
                item.CreatedBy = v.CreatedBy.UserName;
                item.UpdateBy = v.UpdatedBy.UserName;
                item.Pabrikan = v.Factory;
                variantVm.Add(item);
            });

            // var products = productRepository.GetProducts();

            // Searching Data
            IEnumerable<ApiVariantVM> filteredProducts;
            if (dr.Search.Value != "" || dr.Search.Value != null)
            {
                var searchText = dr.Search.Value.Trim();

                filteredProducts = variantVm.Where(v =>
                        v.Name.Contains(searchText));
            }
            else
            {
                filteredProducts = variantVm;
            }

            // Sort Data
            if (dr.Order.Any())
            {
                int sortColumnIndex = dr.Order[0].Column;
                string sortDirection = dr.Order[0].Dir;

                Func<ApiVariantVM, string> orderingFunctionString = null;
                Func<ApiVariantVM, int> orderingFunctionInt = null;
                Func<ApiVariantVM, decimal?> orderingFunctionDecimal = null;

                switch (sortColumnIndex)
                {
                    case 0:     // ProductID
                        {
                            orderingFunctionInt = (c => c.Id);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionInt)
                                    : filteredProducts.OrderByDescending(orderingFunctionInt);
                            break;
                        }
                    case 1:     // ProductName
                        {
                            orderingFunctionString = (c => c.Name);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionString)
                                    : filteredProducts.OrderByDescending(orderingFunctionString);
                            break;
                        }
                }
            }

            // Paging Data
            var pagedProducts = filteredProducts.Skip(dr.Start).Take(dr.Length);

            return new DataTableResponse
            {
                draw = dr.Draw,
                recordsTotal = variantVm.Count(),
                recordsFiltered = variantVm.Count(),
                data = pagedProducts.ToArray(),
                error = ""
            };

        }
    }
}