﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Bengkel.Models;
using Bengkel.Util;

namespace Bengkel.Controllers.api
{
    public class DummyApisController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/DummyApis
        public IQueryable<DummyApi> GetDummyApis()
        {
            return db.DummyApis;
        }

        // GET: api/DummyApis/5
        [ResponseType(typeof(DummyApi))]
        public async Task<IHttpActionResult> GetDummyApi(int id)
        {
            DummyApi dummyApi = await db.DummyApis.FindAsync(id);
            if (dummyApi == null)
            {
                return NotFound();
            }

            return Ok(dummyApi);
        }

        // PUT: api/DummyApis/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDummyApi(int id, DummyApi dummyApi)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dummyApi.Id)
            {
                return BadRequest();
            }

            db.Entry(dummyApi).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DummyApiExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DummyApis
        [ResponseType(typeof(DummyApi))]
        public async Task<IHttpActionResult> PostDummyApi(DummyApi dummyApi)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DummyApis.Add(dummyApi);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = dummyApi.Id }, dummyApi);
        }

        // DELETE: api/DummyApis/5
        [ResponseType(typeof(DummyApi))]
        public async Task<IHttpActionResult> DeleteDummyApi(int id)
        {
            DummyApi dummyApi = await db.DummyApis.FindAsync(id);
            if (dummyApi == null)
            {
                return NotFound();
            }

            db.DummyApis.Remove(dummyApi);
            await db.SaveChangesAsync();

            return Ok(dummyApi);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DummyApiExists(int id)
        {
            return db.DummyApis.Count(e => e.Id == id) > 0;
        }
    }
}