﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Bengkel.Models.mitra;
using Bengkel.Util;
using Bengkel.ViewModel.api;

namespace Bengkel.Controllers.api.mitra
{
    public class SuppliersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Suppliers
        public IQueryable<Supplier> GetSuppliers()
        {
            return db.Suppliers;
        }

        // GET: api/Suppliers/5
        [ResponseType(typeof(Supplier))]
        public async Task<IHttpActionResult> GetSupplier(int id)
        {
            Supplier supplier = await db.Suppliers.FindAsync(id);
            if (supplier == null)
            {
                return NotFound();
            }

            return Ok(supplier);
        }

        // PUT: api/Suppliers/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSupplier(int id, Supplier supplier)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != supplier.Id)
            {
                return BadRequest();
            }

            db.Entry(supplier).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SupplierExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Suppliers
        [ResponseType(typeof(Supplier))]
        public async Task<ResponGeneral> PostSupplier(Supplier supplier)
        {
            var currentUser = await db.Users.Where(x => x.UserName == User.Identity.Name)
                .SingleOrDefaultAsync();

            supplier.CreatedBy = currentUser;
            supplier.UpdatedBy = currentUser;
            db.Suppliers.Add(supplier);

            var result = await db.SaveChangesAsync();
            if (result > 0)
            {
                return new ResponGeneral()
                {
                    status = true,
                    msg = "insert berhasil",
                    data = supplier
                };
            }
            else
            {
                return new ResponGeneral()
                {
                    status = false,
                    msg = "insert gagal",
                    data = null
                };
            }

//            return CreatedAtRoute("DefaultApi", new { id = supplier.Id }, supplier);
        }

        // DELETE: api/Suppliers/5
        [ResponseType(typeof(Supplier))]
        public async Task<IHttpActionResult> DeleteSupplier(int id)
        {
            Supplier supplier = await db.Suppliers.FindAsync(id);
            if (supplier == null)
            {
                return NotFound();
            }

            db.Suppliers.Remove(supplier);
            await db.SaveChangesAsync();

            return Ok(supplier);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SupplierExists(int id)
        {
            return db.Suppliers.Count(e => e.Id == id) > 0;
        }

        [System.Web.Http.Route("api/getSupplierDataTable")]
        public DataTableResponse getDataTable(DataTableRequest dr)
        {
            var products = db.Suppliers.Where(s => s.IsDeleted == false).ToList();
            List<ApiSupplierVM> supplierVms = new List<ApiSupplierVM>();
            products.ForEach(s =>
            {
                var item = new ApiSupplierVM();
                item.Id = s.Id;
                item.Name = s.Name;
                item.Created = s.Created;
                item.Updated = s.Updated;
                item.isDel = s.IsDeleted;
                if (s.CreatedBy.UserName != null)
                {
                    item.CreatedBy = s.CreatedBy.UserName;
                }
                if (s.UpdatedBy.UserName != null)
                {
                    item.UpdateBy = s.UpdatedBy.UserName;
                }
                supplierVms.Add(item);
            });

            // var products = productRepository.GetProducts();

            // Searching Data
            IEnumerable<ApiSupplierVM> filteredProducts;
            if (dr.Search.Value != "" || dr.Search.Value != null)
            {
                var searchText = dr.Search.Value.Trim();

                filteredProducts = supplierVms.Where(s =>
                        s.Name.Contains(searchText));
            }
            else
            {
                filteredProducts = supplierVms;
            }

            // Sort Data
            if (dr.Order.Any())
            {
                int sortColumnIndex = dr.Order[0].Column;
                string sortDirection = dr.Order[0].Dir;

                Func<ApiSupplierVM, string> orderingFunctionString = null;
                Func<ApiSupplierVM, int> orderingFunctionInt = null;
                Func<ApiSupplierVM, decimal?> orderingFunctionDecimal = null;

                switch (sortColumnIndex)
                {
                    case 0:     // ProductID
                        {
                            orderingFunctionInt = (c => c.Id);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionInt)
                                    : filteredProducts.OrderByDescending(orderingFunctionInt);
                            break;
                        }
                    case 1:     // ProductName
                        {
                            orderingFunctionString = (c => c.Name);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionString)
                                    : filteredProducts.OrderByDescending(orderingFunctionString);
                            break;
                        }
                }
            }

            // Paging Data
            var pagedProducts = filteredProducts.Skip(dr.Start).Take(dr.Length);

            return new DataTableResponse
            {
                draw = dr.Draw,
                recordsTotal = supplierVms.Count(),
                recordsFiltered = supplierVms.Count(),
                data = pagedProducts.ToArray(),
                error = ""
            };

        }
    }
}