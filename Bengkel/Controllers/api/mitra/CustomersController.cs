﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Bengkel.Models.mitra;
using Bengkel.Util;
using Bengkel.ViewModel.api;

namespace Bengkel.Controllers.api.mitra
{
    public class CustomersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Customers
        public IQueryable<Customer> GetCustomers()
        {
            return db.Customers;
        }

        // GET: api/Customers/5
        [ResponseType(typeof(Customer))]
        public async Task<IHttpActionResult> GetCustomer(int id)
        {
            Customer customer = await db.Customers.FindAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            return Ok(customer);
        }

        // PUT: api/Customers/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCustomer(int id, Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != customer.Id)
            {
                return BadRequest();
            }

            db.Entry(customer).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Customers
        [ResponseType(typeof(Customer))]
        public async Task<ResponGeneral> PostCustomer(ApiCustomerVM customeVM)
        {
            var currentUser = await db.Users.Where(x => x.UserName == User.Identity.Name)
                .SingleOrDefaultAsync();
            var curTime = DateTimeOffset.UtcNow;
            if (!ModelState.IsValid)
            {
                return new ResponGeneral()
                {
                    status = false,
                    msg = "model invalid",
                    data = customeVM
                };
            }
            var varCust = await db.Variants.FindAsync(customeVM.Variant.ID);
            Customer customer = new Customer();
            customer.NoPol = customeVM.NoPol;
            customer.Tahun = customeVM.Tahun;
            customer.Owner = customeVM.Owner;
            customer.Variant = varCust;
            customer.CreatedBy = currentUser;
            customer.UpdatedBy = currentUser;
            customer.Created = curTime;
            customer.Updated = curTime;
            
            db.Customers.Add(customer);
            var result = await db.SaveChangesAsync();
            if (result > 0)
            {
                return new ResponGeneral()
                {
                    status = true,
                    msg = "insert berhasil",
                    data = customer
                };
            }
            else
            {
                return new ResponGeneral()
                {
                    status = false,
                    msg = "insert gagal",
                    data = customer
                };
            }

//            return CreatedAtRoute("DefaultApi", new { id = customer.Id }, customer);
        }

        // DELETE: api/Customers/5
        [ResponseType(typeof(Customer))]
        public async Task<IHttpActionResult> DeleteCustomer(int id)
        {
            Customer customer = await db.Customers.FindAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            db.Customers.Remove(customer);
            await db.SaveChangesAsync();

            return Ok(customer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CustomerExists(int id)
        {
            return db.Customers.Count(e => e.Id == id) > 0;
        }


        
        [System.Web.Http.Route("api/getCustDataTable")]
        public DataTableResponse getCustDataTable(DataTableRequest dr)
        {
            var products = db.Customers.Include("Variant").Where(v => v.IsDeleted == false).ToList();
            List<ApiCustomerVM> custVm = new List<ApiCustomerVM>();
            products.ForEach(c =>
            {
                var item = new ApiCustomerVM();
                item.Id = c.Id;
                item.NoPol = c.NoPol;
                item.Owner = c.Owner;
                item.Created = c.Created;
                item.Updated = c.Updated;
                item.CreatedBy = c.CreatedBy.UserName;
                item.UpdateBy = c.UpdatedBy.UserName;
                item.Tahun = c.Tahun;
                item.Variant = c.Variant;
                custVm.Add(item);
            });

            // var products = productRepository.GetProducts();

            // Searching Data
            IEnumerable<ApiCustomerVM> filteredProducts;
            if (dr.Search.Value != "" || dr.Search.Value != null)
            {
                var searchText = dr.Search.Value.Trim();

                filteredProducts = custVm.Where(p =>
                        p.NoPol.Contains(searchText));
            }
            else
            {
                filteredProducts = custVm;
            }

            // Sort Data
            if (dr.Order.Any())
            {
                int sortColumnIndex = dr.Order[0].Column;
                string sortDirection = dr.Order[0].Dir;

                Func<ApiCustomerVM, string> orderingFunctionString = null;
                Func<ApiCustomerVM, int> orderingFunctionInt = null;
                Func<ApiCustomerVM, decimal?> orderingFunctionDecimal = null;

                switch (sortColumnIndex)
                {
                    case 0:     // ProductID
                        {
                            orderingFunctionInt = (c => c.Id);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionInt)
                                    : filteredProducts.OrderByDescending(orderingFunctionInt);
                            break;
                        }
                    case 1:     // ProductName
                        {
                            orderingFunctionString = (c => c.NoPol);
                            filteredProducts =
                                sortDirection == "asc"
                                    ? filteredProducts.OrderBy(orderingFunctionString)
                                    : filteredProducts.OrderByDescending(orderingFunctionString);
                            break;
                        }
                }
            }

            // Paging Data
            var pagedProducts = filteredProducts.Skip(dr.Start).Take(dr.Length);

            return new DataTableResponse
            {
                draw = dr.Draw,
                recordsTotal = custVm.Count(),
                recordsFiltered = custVm.Count(),
                data = pagedProducts.ToArray(),
                error = ""
            };

        }
    }
}