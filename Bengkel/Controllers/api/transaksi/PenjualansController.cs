﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Bengkel.Models.transaksi;
using Bengkel.Util;
using Bengkel.ViewModel.api;

namespace Bengkel.Controllers.api.transaksi
{
    public class PenjualansController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Penjualans
        public IQueryable<Penjualan> GetPenjualans()
        {
            return db.Penjualans;
        }

        // GET: api/Penjualans/5
        [ResponseType(typeof(Penjualan))]
        public async Task<IHttpActionResult> GetPenjualan(int id)
        {
            Penjualan penjualan = await db.Penjualans.FindAsync(id);
            if (penjualan == null)
            {
                return NotFound();
            }

            return Ok(penjualan);
        }

        // PUT: api/Penjualans/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPenjualan(int id, Penjualan penjualan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != penjualan.Id)
            {
                return BadRequest();
            }

            db.Entry(penjualan).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PenjualanExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Penjualans
        [ResponseType(typeof(Penjualan))]
        public async Task<IHttpActionResult> PostPenjualan(Penjualan penjualan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Penjualans.Add(penjualan);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = penjualan.Id }, penjualan);
        }

        // DELETE: api/Penjualans/5
        [ResponseType(typeof(Penjualan))]
        public async Task<IHttpActionResult> DeletePenjualan(int id)
        {
            Penjualan penjualan = await db.Penjualans.FindAsync(id);
            if (penjualan == null)
            {
                return NotFound();
            }

            db.Penjualans.Remove(penjualan);
            await db.SaveChangesAsync();

            return Ok(penjualan);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PenjualanExists(int id)
        {
            return db.Penjualans.Count(e => e.Id == id) > 0;
        }

        [HttpPost]
        [Route("api/ceknopol")]
        public async Task<ResponGeneral> cekNoPol([FromBody] ResponGeneral respon)
        {
            if (respon.status)
            {

                var cust = await db.Customers.Include("Variant").Where(customer => customer.NoPol.Trim()
                    .ToLower().Contains(respon.msg.Trim().ToLower())).SingleOrDefaultAsync();
                if (cust != null)
                {
                    return new ResponGeneral()
                    {
                        status = true,
                        msg = "unit terdaftar",
                        data = cust
                    };
                }
                
                return new ResponGeneral()
                {
                    status = false,
                    msg = "data tidak ditemukan"
                };

            }
            
            return new ResponGeneral()
            {
                status = false,
                msg = "format requets salah"
            };
        }

        [HttpPost]
        [Route("api/pushJual")]
        public async Task<ResponGeneral> pushJual([FromBody]GeneralRespPenjualan responGeneral)
        {
            //            PushDetailDataVM item = JsonConvert.DeserializeObject<PushDetailDataVM>(pushdetailBelis.ToString());
            List<ApiDetailJualVM> detailJualVms = new List<ApiDetailJualVM>();
            detailJualVms = responGeneral.datas;
            var currentUser = await db.Users.Where(x => x.UserName == User.Identity.Name)
                .SingleOrDefaultAsync();
            var currCust = await db.Customers.FindAsync(responGeneral.IdCust);
            Penjualan jual = new Penjualan();
            jual.Customer = currCust;
            jual.TglJual = DateTimeOffset.UtcNow;
            jual.ServiceBy = currentUser;

            db.Penjualans.Add(jual);

            var result = await db.SaveChangesAsync();

            if (result > 0)
            {
                foreach (var apiDetailJualVm in detailJualVms)
                {
                    var detJual = new DetailJual();
                    detJual.Penjualan = jual;
                    detJual.JmlJual = apiDetailJualVm.JmlJual;
                    var part = await db.Parts.FindAsync(apiDetailJualVm.Part);
                    detJual.Part = part;
                    db.DetailJuals.Add(detJual);
                    var rslt = await db.SaveChangesAsync();
                    if (rslt < 1)
                    {
                        return new ResponGeneral()
                        {
                            status = false,
                            msg = "gagal dalam insert detail"
                        };
                    }
                }
                return new ResponGeneral()
                {
                    status = true,
                    msg = "insert berhasil"
                };

            }
            else
            {
                return new ResponGeneral()
                {
                    status = false,
                    msg = "gagal dalam menambahkan pembelian"
                };
            }
        }
    }
}