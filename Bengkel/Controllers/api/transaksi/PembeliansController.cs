﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Bengkel.Models.transaksi;
using Bengkel.Util;
using Bengkel.ViewModel.api;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Bengkel.Controllers.api.transaksi
{
    public class PembeliansController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Pembelians
        public IQueryable<Pembelian> GetPembelians()
        {
            return db.Pembelians;
        }

        // GET: api/Pembelians/5
        [ResponseType(typeof(Pembelian))]
        public async Task<IHttpActionResult> GetPembelian(int id)
        {
            Pembelian pembelian = await db.Pembelians.FindAsync(id);
            if (pembelian == null)
            {
                return NotFound();
            }

            return Ok(pembelian);
        }

        // PUT: api/Pembelians/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPembelian(int id, Pembelian pembelian)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pembelian.Id)
            {
                return BadRequest();
            }

            db.Entry(pembelian).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PembelianExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Pembelians
        [ResponseType(typeof(Pembelian))]
        public async Task<IHttpActionResult> PostPembelian(Pembelian pembelian)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Pembelians.Add(pembelian);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = pembelian.Id }, pembelian);
        }

        // DELETE: api/Pembelians/5
        [ResponseType(typeof(Pembelian))]
        public async Task<IHttpActionResult> DeletePembelian(int id)
        {
            Pembelian pembelian = await db.Pembelians.FindAsync(id);
            if (pembelian == null)
            {
                return NotFound();
            }

            db.Pembelians.Remove(pembelian);
            await db.SaveChangesAsync();

            return Ok(pembelian);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PembelianExists(int id)
        {
            return db.Pembelians.Count(e => e.Id == id) > 0;
        }

        [Route("api/getSuppDropDown")]
        public async Task<IEnumerable<ApiSupplierDropDown>> getFactoryDropDown()
        {
            List<ApiSupplierDropDown> lists = new List<ApiSupplierDropDown>();
            var facts = await db.Suppliers.Where(factory => factory.IsDeleted == false).ToListAsync();
            facts.ForEach(factory =>
            {
                var item = new ApiSupplierDropDown();
                item.Id = factory.Id;
                item.Name = factory.Name;
                lists.Add(item);
            });
            return lists;
        }

        [HttpPost]
        [Route("api/pushCart")]
        public async Task<ResponGeneral> pushCart([FromBody]PushDetailDataVM pushdetailBelis)
        {
//            PushDetailDataVM item = JsonConvert.DeserializeObject<PushDetailDataVM>(pushdetailBelis.ToString());
            List<ApiDetailBeliVM> detailBelis = new List<ApiDetailBeliVM>();
            detailBelis = pushdetailBelis.data;
            var currentUser = await db.Users.Where(x => x.UserName == User.Identity.Name)
                .SingleOrDefaultAsync();
            Pembelian beli = new Pembelian();
            beli.Name = DateTimeOffset.UtcNow.ToString();
            beli.tglBeli = DateTimeOffset.UtcNow;
            beli.Reciever = currentUser;

            db.Pembelians.Add(beli);

            var result = await db.SaveChangesAsync();

            if (result > 0)
            {
                foreach (var apiDetailBeliVm in detailBelis)
                {
                    var detBeli = new DetailBeli();
                    detBeli.Pembelian = beli;
                    var sup = await db.Suppliers.FindAsync(apiDetailBeliVm.SupplierId);
                    detBeli.Supplier = sup;
                    detBeli.jmlBeli = apiDetailBeliVm.jmlBeli;
                    var part = await db.Parts.FindAsync(apiDetailBeliVm.PartId);
                    detBeli.Part = part;
                    db.DetailBelis.Add(detBeli);
                    var rslt = await db.SaveChangesAsync();
                    if (rslt < 1)
                    {
                        return new ResponGeneral()
                        {
                            status = false,
                            msg = "gagal dalam insert detail"
                        };
                    }
                }
                return new ResponGeneral()
                {
                    status = true,
                    msg = "insert berhasil"
                };

            }
            
            return new ResponGeneral()
            {
                status = false,
                msg = "gagal dalam menambahkan pembelian"
            };
        }
    }
}