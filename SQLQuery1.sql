﻿Create Trigger BarangMasuk on DetailBelis
for insert
as
	declare @stok as int
	declare @jum_beli as int
	declare @kodebarang as int
	select @kodebarang=Part_ID,@jum_beli=jmlBeli from inserted
	select @stok=Stock from Parts where ID=@kodebarang
	set @stok=@stok+@jum_beli
	begin transaction
	update Parts set Stock=@stok where ID=@kodebarang
	if @@error=0
	commit transaction
	else
	rollback transaction